const chai = require('chai');
const {assert} = require('chai');

//Import and use chai-http to allow chai to send requests to our server.
const http = require('chai-http');
chai.use(http);

describe('api_test_suite_users', ()=>{

        //.request() method is used by chai to create an http request to the given server.
		//.get("/endpoint") is used to run/access a get method route
		//.end() which is used to access the response of from the route. It has an anonymous function as an argument which handles 2 objects, the error and response object which we can assert a condition with.
    it("test_api_get_users_is_running", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res)=>{

            //isDefined() is a an assertion that the given object/data is NOT undefined like a shortcut to notEqual(typeof(),undefined)
            assert.isDefined(res);
            //done() method is used to tell chai http when the test is done.
            done();
        })
    })

    it("test_api_get_users_returns_array", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res)=>{

            //res.body contains the body of the response. The data sent from res.send()
            //console.log(res.body);

            //isArray() is an assertion that the given data is an array.
            assert.isArray(res.body);
            done();
        })
    })

    it("test_api_get_users_first_item_is_john", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res)=>{

            //console.log(res.body[0]);
            assert.equal(res.body[0].name,"John");
            done();
        })
    })

 

/* 
    Mini-Activity:

    Create a test case to check/confirm the last item in the users array.

*/
    it("test_api_get_users_check_the_last_item_in_the_users_array_is_not_undefined", (done)=>{

        chai.request("http://localhost:4000")
        .get("/users")
        .end((err,res)=>{

            //console.log(res.body[res.body.length - 1]);
           assert.notEqual(res.body[res.body.length - 1],undefined);
           done();
        })

    })
   

// 2. In your test.js, Add following test cases in the api_test_suite_users:

    // a. Check if the post endpoint is running.
    it("test_api_get_users_is_running", (done)=>{

        chai.request("http://localhost:4000")
        .post("/users")
        .end((err,res)=>{

            assert.isDefined(res);
            done();
        })
    })

    it("test_api_post_users_returns_400_if_no_name",(done) =>{

        //.post() which is used by chai http to access a post method route
        //.type() which is used to tell chai that the request body is going to be stringified as json
        //.send() is used to send the request body
        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            age:31,
            username: "irene91"
        })
        .end((err,res)=>{
            assert.equal(res.status,400);
            done();
        })
    })

    // b. Check if the post endpoint encounters an error if there is no username.
    it("test_api_post_users_returns_400_if_no_username",(done) =>{

     
        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            age:31,
            username: "irene91"
        })
        .end((err,res)=>{
            assert.equal(res.status,400);
            done();
        })
    })
   

    // c. Check if the post endpoint encounters an error if there is no age.
    it("test_api_post_users_returns_400_if_no_age",(done) =>{

        chai.request("http://localhost:4000")
        .post("/users")
        .type("json")
        .send({
            age:31,
            username: "irene91"
        })
        .end((err,res)=>{
            assert.equal(res.status,400);
            done();
        })
    })

});

describe('api_test_suite_products', ()=>{      

   //2. Create a new test suite with 3 test cases
            
    it("test_api_get_products_is_running", (done)=>{

        chai.request("http://localhost:4000")
        .get("/products")
        .end((err,res)=>{
            // -test if the /products route is running. Assert that the res object should not be undefined.
            assert.isDefined(res);
            done();
        })

    })

    it("test_api_get_products_returns_array", (done)=>{

        chai.request("http://localhost:4000")
        .get("/products")
        .end((err,res)=>{

            // -test if the /products route is returning an array. Assert that the res.body is an array.
            assert.isArray(res.body);
            done();
        })

    })

    it("test_api_get_products_check_the_first_item_in_the_products_array_is_an_object", (done)=>{

        chai.request("http://localhost:4000")
        .get("/products")
        .end((err,res)=>{

            // -test that the first item in the array is an object.

            //assert.equal(typeof(res.body[0]),"object")

            //.isObject() assertion is used to assert that the type of data passed is an object.
            assert.isObject(res.body[0]);
            done();
        })

    })

});




