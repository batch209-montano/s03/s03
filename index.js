/* 
    Mini-Activity

    Create an expressjs api running at port 4000.

    Make sure to add a message that the server is running.


*/

const express = require('express');

const app = express();

const PORT = 4000;

app.use(express.json());

let users = [
    {
        name: "John",
        age: 18,
        username: "johnsmith99"
    },

    {
        name: "John",
        age: 21,
        username: "johnson1991"
    },

    {
        name: "Smith",
        age: 19,
        username: "smithMike12"
    }

];

app.get('/users', (req, res) => {
	return res.send(users);
});
/* 
    Mini-Activity:

 
1. In index.js
-initialize an array of objects for products. Each object should have the following fields:
-name
-price
-isActive

- Create a get method ith the "/products" endpoint that returns the products array.
*/

let products = [
    {
        name: "Sopas",
        price: 25,
        isActive: true
    },

    {
        name: "Lugaw",
        price: 20,
        isActive: true
    },

    {
        name: "Champorado",
        price: 15,
        isActive: true
    }

];

app.get('/products', (req, res) => {
	return res.send(products);
});



app.post('/users', (req, res) => {

    //add a simple if statement that if the request body does not have a name property we will send an error message along with a 400 http status code. (Bad Request).
    //.hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object.
    if (!req.body.hasOwnProperty("name")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter NAME"
        })
    } 

    if (!req.body.hasOwnProperty("age")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter AGE"
        })
    } 

// Activity

// 1. Add a another if statement in the /users post route that returns and sends 400 status and error message if the request body does not have a username field.

    if (!req.body.hasOwnProperty("username")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter username"
        })
    } 

	
});




app.listen(PORT, () => {
	console.log('Running on port ' + PORT);
})